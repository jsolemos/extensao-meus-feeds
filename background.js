(function(){
	var bg = {
		name:'meus_feeds',
		db:null,
		rssList:[],
		result:[],
		alarm:{
			name:'alarm_meus_feeds',
			config:{
				delayInMinutes:1,
				periodInMinutes:1
			}
		},
		timeout:{
			delay:60000,
			func:null
		},
		countProcessados:0,
		countCarregados:0,
		montaObjDados:function(obj){
			return $.extend(true,{last_check_updates:this.last_check_updates},obj);
		},
		getRss: function(url, callback){
			return $.get(url)
			.done($.proxy(callback,this,url))
			.error($.proxy(callback,this,url));
		},
		checkAtualizacoes:function(){
			var updates = 0;
			
			$.each(this.rssList, $.proxy(function(i, feed){				
				if(this.result.last_check_updates.time < feed.getLastUpdate().time){
					updates++;
				}
			},this));
			if(updates > 0)
				chrome.browserAction.setBadgeText({text:""+updates});
			
			this.last_check_updates = this.result.last_check_updates;				
			this.db.set(this.montaObjDados({"url_feeds":this.result.url_feeds}), function(){});
		},
		addFeedToRssList:function(url,response){
			++this.countCarregados;	
			this.rssList.push(new Feed(response,url));
			if(this.countCarregados >= this.result.url_feeds.length)
				$.proxy(this.checkAtualizacoes(),this);
		},
		getFeedList:function(callback){
			//var listStr = window.localStorage.getItem("url_feeds");
			this.db.get.apply(this,[["last_check_updates","url_feeds"], function(results, callback, context){
				callback.apply(context,[results]);
			},callback,this]);
		},
		execute:function(result){
			/*if(this.timeout.func != null)
				clearTimeout(this.timeout.func);*/
				this.result = result;
				for(index in this.result.url_feeds){
					var urlf = this.result.url_feeds[index].url;
					this.getRss(urlf, this.addFeedToRssList);
				}
			//this.timeout.func = setTimeout($.proxy(this.start,this),this.timeout.delay);
		},
		start:function(e){
				this.getFeedList($.proxy(function(list){
					//console.log(list, arguments);
					this.execute.apply(this,arguments);
				},this));
			
		},
		checkAlarm:function(alarm){
			if(alarm.name == this.alarm.name){
				this.start();
			}
			
		},
		last_check_updates:{
			last_check_updates:{
				relative:"",
				time:0
			}
		},
		setAlarms:function(){
			chrome.alarms.create(this.alarm.name,this.alarm.config);
			
			chrome.alarms.onAlarm.addListener($.proxy(this.checkAlarm,this));
		},
		init:function(){
			this.db = new DB(this.name);
			this.start();
			this.setAlarms();
			chrome.browserAction.setBadgeText({text:"BG"});
		}
	};
	
	bg.init();
})();
