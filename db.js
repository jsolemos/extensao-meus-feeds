﻿function DB(db_name){
	var DB = {
		results:{},
		listTablesResultIsArray:['url_feeds'],
		db_name:db_name,
		defaultCallback: {
			get:function(dados){
				return dados;
			},
			set:function(){
				console.log("operação realizada");
			}
		},
		returnValueToTable:function(table){
			return $.inArray(table, DB.listTablesResultIsArray) != -1 ? [] : {};
		},
		filterResult: function(obj,callback,callback2,dados){
			//console.log(obj,callback,callback2,dados);
			var results = {};
			var db_results = dados[DB.db_name];
			
			switch(typeof(obj)){
				case "object":
					$.each(obj, function(i,table){
						if(db_results !== undefined && db_results[table] !== undefined){
							results[table] = db_results[table];
						}else{
							results[table] = DB.returnValueToTable(table);						
						}
					});
				break;
				case "string":
					if(db_results !== undefined && db_results[obj] !== undefined){
						results[obj] = db_results[obj];
					}else{
						results[obj] = DB.returnValueToTable(obj);						
					}
				break;
			}
			callback.apply(this,[results, callback2]);
		},
		get:function(obj,callback,callback2){
			var context = this;
			chrome.storage.sync.get(DB.db_name,function(dados){
				DB.filterResult.apply(context,[obj,callback,callback2,dados]);
				//callback.apply(this,[dados, callback2]);
			});
		},
		set:function(obj,callback){
			var dados = {};
			dados[this.db_name] = obj;
			return chrome.storage.sync.set(dados,callback || this.defaultCallback.set);
		},
		getDados:function(){
						
		},
		init: function(){
			return this;
		}
		
	};
	return DB.init();
}