function Feed(rss,url){
	var feed = {
		rss:null,
		urlRss:null,
		getLogo:function(){
			return this.rss.find("channel > image > url").text() || false;
		},
		getTitulo:function(){
			return this.rss.find("channel > title").text() || this.getLink();
		},
		getLink:function(){
			return this.rss.find("channel > link").text();
		},
		getItems:function(){
			this.items = this.rss.find("channel > item").get();
			$.each(this.items, $.proxy(function(i,post){
				this.itemsObjs.push(this.getItem(i));
			},this));
		},
		getLastUpdate: function(){
			
			lastDate = new Date($(this.items[0]).find("pubDate").text());			
			lastDate = moment(lastDate).locale('pt-br');
			
			return {
				relative:lastDate.fromNow(),
				time:lastDate.unix()
			};
		},
		items: [],
		itemsObjs:[],
		getItem:function(index){
			var item = $(this.items[index]);
			if(item !== undefined){
				var date = new Date(item.find("pubDate").text());
				var data_postagem = moment(date).locale('pt-br');
				var itemFeed = {
					titulo_postagem:item.find("title").text(),
					link: (item.find("guid").text() || item.find("link").text()),
					data_postagem:data_postagem.format("DD/MM/YY HH:mm")+" ("+data_postagem.fromNow()+")",
					descricao:item.find("description").text()
				};
				return itemFeed;
			}
			return null;
		},
		init:function(rss,url){
			this.urlRss = url;
			this.rss = $(rss).find("rss");
			this.getItems();
			
			return this;
		}
	};
	
	return feed.init(rss,url);
	
}