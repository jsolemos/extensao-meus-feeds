﻿//$(function(){
$(function(){	
	var prog = {
		name:'meus_feeds',
		db:null,
		rssList:[],
		templateFeed:'',
		rssObj: [],
		qtdLastPosts: 5,
		getTemplates:function(){
			this.templateFeed = $("#feedsRss").html();
			$("#feedsRss").html("");
		},
		setLoading: function(show){
			$loading = $("#loading");
			if(show){
				$loading.show();
			}else{
				$loading.hide();
			}
		},
		orderFeeds:function(a,b){
			return a.last_update.time < b.last_update.time;
		},
		countProcessados:0,
		countCarregados:0,
		reprocessa:function(){
			//$("#feedsRss").html("");
			this.countProcessados--;
			//this.processa();
		},
		processa: function(){	
			
			for(i=this.countProcessados,tam = this.rssList.length; i < tam;i++){
				var feed = this.rssList[i];
				var lastPostagens = feed.itemsObjs.length > this.qtdLastPosts ? feed.itemsObjs.splice(0,this.qtdLastPosts) : feed.itemsObjs;
				lastPostagens.push({
					titulo_postagem:"<strong>Ver todas as postagens</strong>",
					link:feed.getLink(),
					descricao:"",
					link_ver_mais:true
				});
				var itemFeed = {
					id_feed:i,
					titulo: feed.getTitulo(),
					url:feed.urlRss,
					last_update:feed.getLastUpdate(),
					qtd_reg:feed.items.length,
					logo:feed.getLogo(),
					items: lastPostagens
				};
				
				this.rssObj.push(itemFeed);
				this.countProcessados++;
			}
			this.rssObj = this.rssObj.sort(this.orderFeeds);
			this.render();			
		},
		render:function(){
			var html = Mustache.render(this.templateFeed, {feeds:this.rssObj});
			this.setLoading(false);
			$feedRssContainer = $("#feedsRss");
			$feedRssContainer.html(html).css({opacity:1}).promise().done(function(){
				$feedRssContainer.collapse();
			});
		},
		getRss: function(url, callback){
			return $.get(url)
			.done($.proxy(callback,this,url))
			.error($.proxy(callback,this,url));
		},
		montaObjDados:function(obj){
			return $.extend(true, this.last_check_updates,obj);
		},
		addFeedToRssList:function(url,response){
			++this.countCarregados;		
			this.rssList.push(new Feed(response,url));
			window.rss = this;			
			if(this.countCarregados >= this.feedList.length)
				$.proxy(this.processa(),this);
		},
		getFeedList:function(callback){
			//var listStr = window.localStorage.getItem("url_feeds");
			this.db.get.apply(this,["url_feeds", function(listStr, callback,context){
				listStr = listStr['url_feeds'];
				callback.apply(context,[((listStr === "" || listStr === null) ? [] : listStr)]);
			},callback,this]);
		},
		addFeedList:function(url, callback){
			//console.log(arguments);
			var lastDate = new moment().locale('pt-br');
			var objFeed = {
				last_update:{
					relative:lastDate.fromNow(),
					time:lastDate.unix()
				},
				url:url
			};
			if(this.inFeedList(url) == -1){
				this.feedList.push(objFeed);				
				//window.localStorage.setItem("url_feeds",JSON.stringify(atualList));
				this.setCheckedUpdates(callback);
				//this.db.set(this.montaObjDados({"url_feeds":this.feedList}), callback);
			}				
				
		},
		removeFeedList:function(url){
			
				var index = {
					ordenado: -1,
					insert: -1
				};
				$.each(this.feedList,function(i, feed){
					if(feed.url == url)
						index.insert = i;
				});
				
				$.each(this.rssObj,function(i, feed){
					if(feed.url == url)
						index.ordenado = i;
				});

				if(index.ordenado > -1 && index.insert > -1){
					this.rssList.length == 1 ? this.rssList = [] : this.rssList.splice(index.insert,1);		
					this.feedList.length == 1 ? this.feedList = [] : this.feedList.splice(index.insert,1);					 
					this.rssObj.length == 1 ? this.rssObj = [] : this.rssObj.splice(index.ordenado,1);					 
					//window.localStorage.setItem("url_feeds",JSON.stringify(atualList));
					
					this.db.set({"url_feeds":this.feedList});
					return true;
				}
				
				return false;
			
		},
		feedList:[],
		regexURL : /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/,
		urlIsValid:function(url){
			return url.match(this.regexUrl);
		},
		cleanInvalidFeedList: function(callback){
			this.getFeedList($.proxy(function(atualList){
				this.feedList = atualList;
				for(i in this.feedList){
					if(!this.urlIsValid(this.feedList[i].url))
						this.removeFeedList(this.feedList[i].url);
				}
				callback.apply(this,arguments);
			},this));
		},
		displayNoConnection:function(show){
			show ? $('.no-connection').addClass("show") : $('.no-connection').removeClass("show");
		},
		connectionsErros:[],
		setEvents:function(){
			$.ajaxSetup({timeout: 5000});
			$(document).ajaxError($.proxy(function (e, jqXHR, ajaxSettings, thrownError) {
				if (jqXHR.status === 0 || jqXHR.readyState === 0) {
					this.connectionsErros.push(ajaxSettings.url);
					
					if(this.connectionsErros.length >= this.feedList.length)
						this.displayNoConnection(true);
				}
			},this));
		
			$(document).on("keypress","#input-add-feed", $.proxy(function(e){
				var $this = $(e.target);
				var url = $this.val();
				
				if(url.indexOf("http://") == -1)
					url = "http://"+url;
					
				if(e.keyCode == 13){
					if(this.urlIsValid(url) && this.inFeedList(url) == -1){
						this.addFeedList.apply(this,[url,$.proxy(function(){
							$this.val("");
							$this[0].value = '';
							this.setLoading(true);
							this.getRss(url, this.addFeedToRssList);
						},this),$this]);
					}
				}
			},this));
			
			$(document).on("click", ".btn-del", $.proxy(function(e){
				e.preventDefault();
				e.stopPropagation();
				var $this = $(e.target);
				var panel_feed = $this.parents('.panel-feed').eq(0);
				var url = panel_feed.data("url");
				if(this.removeFeedList(url)){
					this.reprocessa();
					panel_feed.fadeOut().promise().done(function(){
						panel_feed.remove();
					});
				}
				
			},this));
		},
		inFeedList:function(url){
			return JSON.stringify(this.feedList).indexOf(url);
		},
		badgeClean:function(){
			chrome.browserAction.setBadgeText({text:""});
		},
		last_check_updates:{
			last_check_updates:{
				relative:"",
				time:0
			}
		},
		setCheckedUpdates : function(callback){
			var date = new moment().locale('pt-br');
			this.last_check_updates.last_check_updates.relative = date.fromNow();
			this.last_check_updates.last_check_updates.time = date.unix();
			this.db.set(this.montaObjDados({"url_feeds":this.feedList}),callback);
		},
		started:false,
		init:function(){
			this.db = new DB(this.name);
			this.badgeClean();
			this.setLoading(true);
			this.getTemplates();
			this.cleanInvalidFeedList(function(feedList){
				this.feedList = feedList;
				
				if(this.feedList.length){
					for(index in this.feedList){
						var urlf = this.feedList[index].url;
						if(this.urlIsValid(urlf))
							this.getRss(urlf, this.addFeedToRssList);
					}
				}else{
					this.render();
				}
				
				this.setCheckedUpdates();
			});
			if(!this.started)
				this.setEvents();
		}
	}
	
	prog.init();
	
});
